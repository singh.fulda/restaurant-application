import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'restaurant',
                loadChildren: './restaurant/restaurant.module#RestaurantApplicationRestaurantModule'
            },
            {
                path: 'product',
                loadChildren: './product/product.module#RestaurantApplicationProductModule'
            },
            {
                path: 'ingredients',
                loadChildren: './ingredients/ingredients.module#RestaurantApplicationIngredientsModule'
            },
            {
                path: 'product-category',
                loadChildren: './product-category/product-category.module#RestaurantApplicationProductCategoryModule'
            },
            {
                path: 'customer',
                loadChildren: './customer/customer.module#RestaurantApplicationCustomerModule'
            },
            {
                path: 'product-order',
                loadChildren: './product-order/product-order.module#RestaurantApplicationProductOrderModule'
            },
            {
                path: 'order-item',
                loadChildren: './order-item/order-item.module#RestaurantApplicationOrderItemModule'
            },
            {
                path: 'invoice',
                loadChildren: './invoice/invoice.module#RestaurantApplicationInvoiceModule'
            },
            {
                path: 'shipment',
                loadChildren: './shipment/shipment.module#RestaurantApplicationShipmentModule'
            },
            {
                path: 'notification',
                loadChildren: './notification/notification.module#RestaurantApplicationNotificationModule'
            }
            /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
        ])
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RestaurantApplicationEntityModule {}
