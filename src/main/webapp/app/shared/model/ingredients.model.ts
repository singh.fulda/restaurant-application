import { IProduct } from 'app/shared/model/product.model';

export interface IIngredients {
    id?: number;
    name?: string;
    price?: number;
    products?: IProduct[];
}

export class Ingredients implements IIngredients {
    constructor(public id?: number, public name?: string, public price?: number, public products?: IProduct[]) {}
}
