import { IRestaurant } from 'app/shared/model/restaurant.model';
import { IProduct } from 'app/shared/model/product.model';

export interface IProductCategory {
    id?: number;
    name?: string;
    description?: string;
    restaurant?: IRestaurant;
    products?: IProduct[];
}

export class ProductCategory implements IProductCategory {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public restaurant?: IRestaurant,
        public products?: IProduct[]
    ) {}
}
