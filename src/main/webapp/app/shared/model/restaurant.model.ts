import { IProductCategory } from 'app/shared/model/product-category.model';
import { ICustomer } from 'app/shared/model/customer.model';
import { IInvoice } from 'app/shared/model/invoice.model';

export interface IRestaurant {
    id?: number;
    name?: string;
    addressLine1?: string;
    addressLine2?: string;
    city?: string;
    postcode?: string;
    telephone?: string;
    fax?: string;
    email?: string;
    owner?: string;
    productCategories?: IProductCategory[];
    customers?: ICustomer[];
    invoices?: IInvoice[];
}

export class Restaurant implements IRestaurant {
    constructor(
        public id?: number,
        public name?: string,
        public addressLine1?: string,
        public addressLine2?: string,
        public city?: string,
        public postcode?: string,
        public telephone?: string,
        public fax?: string,
        public email?: string,
        public owner?: string,
        public productCategories?: IProductCategory[],
        public customers?: ICustomer[],
        public invoices?: IInvoice[]
    ) {}
}
