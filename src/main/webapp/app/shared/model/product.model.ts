import { IIngredients } from 'app/shared/model/ingredients.model';
import { IProductCategory } from 'app/shared/model/product-category.model';

export interface IProduct {
    id?: number;
    name?: string;
    description?: string;
    price?: number;
    ingredients?: IIngredients[];
    productCategory?: IProductCategory;
}

export class Product implements IProduct {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public price?: number,
        public ingredients?: IIngredients[],
        public productCategory?: IProductCategory
    ) {}
}
