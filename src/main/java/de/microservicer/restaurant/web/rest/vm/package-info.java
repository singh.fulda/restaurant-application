/**
 * View Models used by Spring MVC REST controllers.
 */
package de.microservicer.restaurant.web.rest.vm;
