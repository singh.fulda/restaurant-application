/**
 * Data Access Objects used by WebSocket services.
 */
package de.microservicer.restaurant.web.websocket.dto;
