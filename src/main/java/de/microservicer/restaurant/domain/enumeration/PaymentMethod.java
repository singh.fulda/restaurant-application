package de.microservicer.restaurant.domain.enumeration;

/**
 * The PaymentMethod enumeration.
 */
public enum PaymentMethod {
    CREDIT_CARD, CASH_ON_DELIVERY, PAYPAL
}
