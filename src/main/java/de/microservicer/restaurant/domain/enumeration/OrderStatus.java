package de.microservicer.restaurant.domain.enumeration;

/**
 * The OrderStatus enumeration.
 */
public enum OrderStatus {
    COMPLETED, PENDING, CANCELLED
}
