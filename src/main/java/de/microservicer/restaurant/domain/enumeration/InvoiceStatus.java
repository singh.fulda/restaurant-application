package de.microservicer.restaurant.domain.enumeration;

/**
 * The InvoiceStatus enumeration.
 */
public enum InvoiceStatus {
    PAID, ISSUED, CANCELLED
}
