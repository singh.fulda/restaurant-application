package de.microservicer.restaurant.domain.enumeration;

/**
 * The NotificationType enumeration.
 */
public enum NotificationType {
    EMAIL, SMS, PARCEL
}
