/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { RestaurantComponentsPage, RestaurantDeleteDialog, RestaurantUpdatePage } from './restaurant.page-object';

const expect = chai.expect;

describe('Restaurant e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let restaurantUpdatePage: RestaurantUpdatePage;
    let restaurantComponentsPage: RestaurantComponentsPage;
    let restaurantDeleteDialog: RestaurantDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Restaurants', async () => {
        await navBarPage.goToEntity('restaurant');
        restaurantComponentsPage = new RestaurantComponentsPage();
        await browser.wait(ec.visibilityOf(restaurantComponentsPage.title), 5000);
        expect(await restaurantComponentsPage.getTitle()).to.eq('restaurantApplicationApp.restaurant.home.title');
    });

    it('should load create Restaurant page', async () => {
        await restaurantComponentsPage.clickOnCreateButton();
        restaurantUpdatePage = new RestaurantUpdatePage();
        expect(await restaurantUpdatePage.getPageTitle()).to.eq('restaurantApplicationApp.restaurant.home.createOrEditLabel');
        await restaurantUpdatePage.cancel();
    });

    it('should create and save Restaurants', async () => {
        const nbButtonsBeforeCreate = await restaurantComponentsPage.countDeleteButtons();

        await restaurantComponentsPage.clickOnCreateButton();
        await promise.all([
            restaurantUpdatePage.setNameInput('name'),
            restaurantUpdatePage.setAddressLine1Input('addressLine1'),
            restaurantUpdatePage.setAddressLine2Input('addressLine2'),
            restaurantUpdatePage.setCityInput('city'),
            restaurantUpdatePage.setPostcodeInput('postcode'),
            restaurantUpdatePage.setTelephoneInput('telephone'),
            restaurantUpdatePage.setFaxInput('fax'),
            restaurantUpdatePage.setEmailInput('email'),
            restaurantUpdatePage.setOwnerInput('owner')
            // restaurantUpdatePage.customerSelectLastOption(),
            // restaurantUpdatePage.invoiceSelectLastOption(),
        ]);
        expect(await restaurantUpdatePage.getNameInput()).to.eq('name');
        expect(await restaurantUpdatePage.getAddressLine1Input()).to.eq('addressLine1');
        expect(await restaurantUpdatePage.getAddressLine2Input()).to.eq('addressLine2');
        expect(await restaurantUpdatePage.getCityInput()).to.eq('city');
        expect(await restaurantUpdatePage.getPostcodeInput()).to.eq('postcode');
        expect(await restaurantUpdatePage.getTelephoneInput()).to.eq('telephone');
        expect(await restaurantUpdatePage.getFaxInput()).to.eq('fax');
        expect(await restaurantUpdatePage.getEmailInput()).to.eq('email');
        expect(await restaurantUpdatePage.getOwnerInput()).to.eq('owner');
        await restaurantUpdatePage.save();
        expect(await restaurantUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await restaurantComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Restaurant', async () => {
        const nbButtonsBeforeDelete = await restaurantComponentsPage.countDeleteButtons();
        await restaurantComponentsPage.clickOnLastDeleteButton();

        restaurantDeleteDialog = new RestaurantDeleteDialog();
        expect(await restaurantDeleteDialog.getDialogTitle()).to.eq('restaurantApplicationApp.restaurant.delete.question');
        await restaurantDeleteDialog.clickOnConfirmButton();

        expect(await restaurantComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
